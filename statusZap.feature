#																																	#
# 	Author: ianrosadas																						#
#		Funcionalidade 2: Usuario adiciona uma foto no Opcao Status		#

Feature: Publicar foto ou vídeo no status do Whatsapp

  Scenario: Acessar o aplicativo
    Given Usuario estar com o aplicativo aberto
    	And Usuario ter clicado na opção de status
    Then Funcionalidade status ativa

  Scenario: Adicionar uma foto no status da galeria de arquivos
    Given Usuario estar na funcionalidade de status
    When Usuário clica no álbum de fotos para selecionar um arquivo
    	And Usuario pode adicionar a legenda nas funcionalidades de edição 
    Then Foto ou vídeo publicado 

  Scenario: Usuário fecha a funcionalidade de status antes de publicar a foto ou vídeo
    Given Usuario nao deseja mais publicar a foto ou vídeo
    When Usuario clica em voltar para fechar a funcionalidade do status
    Then Usuário retorna para a tela anterior à funcionalidade do status
