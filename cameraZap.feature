#																											#
# 	Author: ianrosadas																#
#		Funcionalidade 1: Chamada de vídeo no Whatsapp	  #

Feature:Chamada de video no Whatsapp

  Scenario: O usuario quer fazer uma chamada de vídeo no Whatsapp
    Given O usuario acessa o aplicativo de Whatsapp
    When Usuario escolhe um contato
    And Usuario clica no botao de chamada de video
     And Contato selecionado aceita a chamada de video 
    Then Chamada de video ativa

  Scenario: Encerrar o convite para chamada de video automaticamente
    Given Usuario chama um contato para ligação em video
    When Usuário aguarda o contato atender
    Then Apos um minuto a chamada de video desliga automaticamente

  Scenario: Verificar a inexistência da chamada de video em grupos
    Given Usuario escolhe um grupo para conversar
    When  Usuario abre a conversa do grupo
    Then Usuário verifica que a funcionalidade não está habilitada no grupo
