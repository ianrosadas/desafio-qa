import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;

public class PricingRules {

	private Integer quantidadeA = 0;
	private float precoA = 0;
	private float precoPromocaoA = 0;
	private Integer quantidadeB = 0;
	private float precoB = 0;
	private float precoPromocaoB = 0;
	private Integer quantidadeC = 0;
	private float precoC = 0;
	private Integer quantidadeD = 0;
	private float precoD = 0;
	private float total = 0;
	private Integer itens = 0;
	
	public void somaA() {

		if (precoPromocaoA == 0) {
			precoA = (float) (precoA + 0.50);
			System.out.print("Adiciona ao carrinho...");
			System.out.println("Preco unitario A eh 50 =>> " + precoA);
			precoPromocaoA = 1;
		} else if (precoPromocaoA == 1) {
			precoA = (float) (precoA + 0.50);
			System.out.print("Adiciona ao carrinho...");
			System.out.println("Preco unitario A eh 50 =>> " + precoA);
			precoPromocaoA = 2;
		} else if (precoPromocaoA == 2) {
			precoA = (float) (precoA + 0.30);
			System.out.print("Adiciona ao carrinho...");
			System.out.println("Preco de promocao A com desconto por 30 =>> " + precoA);
			precoPromocaoA = 0;
		}		
	}

	public void somaB() {
		if (precoPromocaoB == 0) {
			precoB = (float) (precoB + 0.30);
			System.out.print("Adiciona ao carrinho...");
			System.out.println("Preco unitario B eh 30 =>> " + precoB);
			precoPromocaoB = 1;
		} else if (precoPromocaoB == 1) {
			precoB = (float) (precoB + 0.15);
			System.out.print("Adiciona ao carrinho...");
			System.out.println("Preco de promocao B com desconto por 15 =>> " + precoB);
			precoPromocaoB = 0;
		}
	}

	public void somaC() {
		precoC = (float) (precoC + 0.20);
		System.out.print("Adiciona ao carrinho...");
		System.out.println("Preco unitario C eh 20 =>> " + precoC);
	}

	public void somaD() {
		precoD = (float) (precoD + 0.15);
		System.out.print("Adiciona ao carrinho...");
		System.out.println("Preco unitario D eh 15 =>> " + precoD);
	}

	public void adicionaItem(String item) {
		
		if (item.equals("A")) {
			quantidadeA = quantidadeA + 1;
			somaA();		
		} else if(item.equals("B")) {
			quantidadeB = quantidadeB + 1;
			somaB();		
		} else if(item.equals("C")) {
			quantidadeC = quantidadeC + 1;
			somaC();
		} else if(item.equals("D")) {
			quantidadeD = quantidadeD + 1;
			somaD();
		}		
	}

	public int quantidadeDeItens() {
		Integer promo = 0;
		Random sorteio = new Random();
		promo = sorteio.nextInt((5)+1);
		return promo;
	}

	public void fechaConta() {
		total = precoA + precoB + precoC + precoD;
		itens = quantidadeA + quantidadeB + quantidadeC + quantidadeD;
		BigDecimal preco = new BigDecimal(total);
		NumberFormat numberformat = NumberFormat.getCurrencyInstance(Locale.US);
		String formatado = numberformat.format(preco);
		System.out.println("###################################################");
		System.out.println("# 				   TOTAL = " + formatado + " #");
		System.out.println("###################################################");
		System.out.println("Fecha a conta!");
		System.out.println("A quantidade da compra ha " + itens + " itens.");
		System.out.println("O valor total a pagar = " + formatado);
		System.out.println("Obrigado, volte sempre!");
	}
}
